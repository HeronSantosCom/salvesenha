program salvaSenha;



uses
  Forms,
  principal.unt in 'principal.unt.pas' {frmPrincipal},
  sobre.unt in 'sobre.unt.pas' {frmSobre},
  tbSys in 'tbSys.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'salvaSenha';
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.CreateForm(TfrmSobre, frmSobre);
  Application.Run;
end.
