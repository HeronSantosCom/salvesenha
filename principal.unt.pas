unit principal.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, CoolTrayIcon, dxCore, dxButton, StdCtrls,
  XP_IconBox, TFlatListBoxUnit, TFlatPanelUnit, BmsXPLabel, TFlatEditUnit,
  Menus, XP_PopUpMenu;

type
  TfrmPrincipal = class(TForm)
    imgFundo: TImage;
    imgLogotipo: TImage;
    imgSimbolo: TImage;
    ctiPadrao: TCoolTrayIcon;
    btnMinimizar: TdxButton;
    btnSobre: TdxButton;
    fplNavegador: TFlatPanel;
    flbNavegador: TFlatListBox;
    btnSair: TdxButton;
    edtBuscar: TFlatEdit;
    lblBuscar: TBmsXPLabel;
    btnBuscar: TdxButton;
    btnInserir: TdxButton;
    btnEditar: TdxButton;
    btnRemover: TdxButton;
    popMenu: TPopupMenu;
    Abrir1: TMenuItem;
    N1: TMenuItem;
    Sair1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btnMinimizarClick(Sender: TObject);
    procedure ctiPadraoDblClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnSobreClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

uses sobre.unt;

{$R *.dfm}

Procedure TfrmPrincipal.FormCreate(Sender: TObject);
begin
  CreateMutex(nil, False, 'SoftCast.OnlyOne');
  if GetLastError = ERROR_ALREADY_EXISTS then begin
    MessageBox(0, 'Este programa j� est� sendo executado!', 'Aviso', MB_ICONSTOP);
    Halt(0); // cancela execu��o
  end;

  ctiPadrao.IconVisible := True;
end;

procedure TfrmPrincipal.btnMinimizarClick(Sender: TObject);
begin
  Application.Minimize;
  ctiPadrao.HideMainForm;
end;

procedure TfrmPrincipal.ctiPadraoDblClick(Sender: TObject);
begin
  ctiPadrao.ShowMainForm;
end;

procedure TfrmPrincipal.btnSairClick(Sender: TObject);
begin
  Halt;
end;

procedure TfrmPrincipal.btnSobreClick(Sender: TObject);
begin
  frmSobre.Show;
end;

end.
